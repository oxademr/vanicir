package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"runtime"
	"strings"
	"time"

	"github.com/ethereum/go-ethereum/common/hexutil"
	"github.com/ethereum/go-ethereum/crypto"
)

var recent, current, totals int64

func searchRate() int64 {
	recent, current = current, 0
	return recent
}

type Wallet struct {
	Address string `json:"address"`
	Key     string `json:"key"`
}

type Matcher struct {
	Prefix string
	Result chan Wallet
}

func (m *Matcher) NewMatch(ctx context.Context, sm chan bool) {

	go func() {

		defer func() {
			if err := recover(); err != nil {
				log.Printf("panic: %v", err)
			}

			<-sm
		}()

		for {
			select {
			case <-ctx.Done():
				return
			default:
				if match := m.Match(); match != nil {
					m.Result <- *match
				}
			}
		}

	}()

}

func (m *Matcher) Match() *Wallet {
	current++
	totals++
	prv, _ := crypto.GenerateKey()
	prvHex := hexutil.Encode(crypto.FromECDSA(prv))
	Addrhex := crypto.PubkeyToAddress(prv.PublicKey).Hex()
	if strings.HasPrefix(Addrhex, m.Prefix) {
		return &Wallet{Address: Addrhex, Key: prvHex}
	}

	return nil
}

func main() {
	var wallets []Wallet
	result := make(chan Wallet, 100)
	matcher := Matcher{
		Prefix: "0x000000",
		Result: result,
	}
	ctx, cancel := context.WithCancel(context.Background())

	go func() {

		tock := time.NewTicker(time.Second)

		for {
			select {
			case <-ctx.Done():
				return
			case <-tock.C:
				fmt.Printf("\rSpeed: %d/s Total: %d", searchRate(), totals)
			case f := <-result:
				wallets = append(wallets, f)
				if len(wallets) == 10 {
					j, _ := json.Marshal(wallets)
					fmt.Println("\n----------------------------------------------------\n")
					fmt.Println(string(j))
					fmt.Println("\n----------------------------------------------------")
					cancel()
					return
				}
			}
		}

	}()

	sm := make(chan bool, runtime.NumCPU())
	for {

		select {
		case <-ctx.Done():
			return
		default:
			sm <- true
			if ctx.Err() == nil {
				matcher.NewMatch(ctx, sm)
			}
		}

	}
}
